package main

import (
	"encoding/binary"
	"flag"
	"io"
	"log"
	"net"
	"os"
	"os/exec"
	"strings"
)

func init() {
	log.SetOutput(os.Stderr)
}

func main() {
	var lAddr, rAddr string
	flag.StringVar(&lAddr, "l", "", "listen addr for server")
	flag.StringVar(&rAddr, "r", "", "remote addr for client")
	flag.Parse()

	if lAddr == "" {
		if rAddr != "" && flag.NArg() >= 2 {
			connect(rAddr, flag.Args())
		} else {
			log.Fatalln("try client but missing args")
		}
	} else {
		listen(lAddr)
	}
}

func listen(addr string) {
	ln, err := net.Listen("tcp", addr)
	abort(err)
	defer ln.Close()
	log.Println("listen at", ln.Addr())
	conn, err := ln.Accept()
	abort(err)
	defer conn.Close()
	log.Println("Client from", conn.RemoteAddr())

	cmdline := readInitialHeader(conn)
	if !(len(cmdline) > 1 && cmdline[0] == "rsync") {
		return
	}
	log.Printf("RUN %s", cmdline)

	cmd := exec.Command(cmdline[0], cmdline[1:]...)
	stdin, err := cmd.StdinPipe()
	abort(err)
	stdout, err := cmd.StdoutPipe()
	abort(err)
	stderr, err := cmd.StderrPipe()
	abort(err)
	abort(cmd.Start())

	var done = make(chan int, 1)
	go func() {
		// conn -> cmd.stdin
		n, e := io.Copy(stdin, conn)
		e = consumeClosedError(e)
		log.Println("net/Rx", n, e)
		done <- 1
	}()
	go func() {
		// cmd.stdout -> conn
		n, e := io.Copy(conn, stdout)
		log.Println("net/Tx", n, e)
		done <- 2
	}()
	// cmd.stderr -> current.stderr
	go io.Copy(os.Stderr, stderr)
	err = cmd.Wait()

	for i := 0; i < 2; i++ {
		j := <-done
		if i == 0 && j == 2 {
			conn.Close()
		}
	}
	if err != nil {
		log.Println("finished", err)
	}
}

func connect(addr string, args []string) {
	conn, err := net.Dial("tcp", addr)
	abort(err)
	defer conn.Close()
	writeInitialHeader(conn, args[1:])

	var done = make(chan int, 1)
	var stdin = getStdin()

	go func() {
		// stdin -> conn
		n, e := io.Copy(conn, stdin)
		log.Println("net/Tx", n, e)
		done <- 1
	}()
	go func() {
		// conn -> stdout
		n, e := io.Copy(getStdout(), conn)
		log.Println("net/Rx", n, e)
		done <- 2
	}()
	for i := 0; i < 2; i++ {
		j := <-done
		if i == 0 && j == 2 {
			if f, y := stdin.(io.Closer); y {
				f.Close()
			}
		}
	}
}

func abort(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

func readInitialHeader(rd net.Conn) []string {
	var l4 = make([]byte, 4)
	_, err := io.ReadFull(rd, l4)
	abort(err)
	nlen := binary.BigEndian.Uint32(l4)
	var cmd = make([]byte, nlen)
	_, err = io.ReadFull(rd, cmd)
	abort(err)
	return strings.Split(string(cmd), "\x00")
}

func writeInitialHeader(rd net.Conn, args []string) {
	var cmdline = strings.Join(args, "\x00")
	var l4 = make([]byte, 4)
	binary.BigEndian.PutUint32(l4, uint32(len(cmdline)))
	_, err := rd.Write(l4)
	abort(err)
	_, err = rd.Write([]byte(cmdline))
	abort(err)
}

func consumeClosedError(e error) error {
	if strings.Contains(e.Error(), "closed") {
		return nil
	} else {
		return e
	}
}
